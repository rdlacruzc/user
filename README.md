# Probar aplicación para registro de usuarios - NISUM
## De preferencia tilizar Sprint tools suite

 - Importar el proyecto a un IDE de desarrollo y configurar jdk 18
 - Actualizar dependencias maven
 - Para correr la aplicación: Clic derecho sobre el proyecto -> Run as -> Spring Boot App
 - Mediante un cliente rest consumir el api POST: http://localhost:8080/user
 - Enviar en el body lo siguiente:
 
```{
   "name":"Juan Rodriguez",
   "email":"juan@rodriguez.com",
   "password":"pa$$w0rD",
   "phones":[
      {
         "number":"1234567",
         "citycode":"1",
         "countrycode":"57"
      },
      {
         "number":"0000000",
         "citycode":"1",
         "countrycode":"50"
      }
   ]
}
```
 - Al momento de iniciar la aplicación se creará las tablas de base de datos en memoria utilizando H2
 - Para acceder a la consola ingrese a: http://localhost:8080/h2-console la jdcb url es jdbc:h2:mem:testdb el usuario es "sa" y sin contraseña.
 - La aplicación valida el formato de correo, que el correo no exista anteriormente y que la contraseña cumpla un patrón específico (El patró será configurable y se puede cambiar en el application.properties)
> La expresión regular para contraseñas tiene la siguiente validación:
- Contiene al menos 6 caracteres.
- Contiene al menos un dígito.
- Contiene al menos un alfabeto en mayúsculas.
- Contiene al menos un alfabeto en minúsculas.
- Contiene al menos un carácter especial que incluye !@#$%&*()-+=^ .
- No contiene ningún espacio en blanco.
> Ejemplo de contraseña: pa$$w0rD

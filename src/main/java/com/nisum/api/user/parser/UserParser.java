package com.nisum.api.user.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.nisum.api.user.dto.PhoneDto;
import com.nisum.api.user.dto.UserRequestDto;
import com.nisum.api.user.dto.UserResponseDto;
import com.nisum.api.user.entity.PhoneEntity;
import com.nisum.api.user.entity.UserEntity;

@Component
public class UserParser {
	

	public UserEntity userDtoToUserEntity(UserRequestDto userDto) {
		UserEntity user = new UserEntity();
		List<PhoneEntity> phones = new ArrayList<>();
		user.setEmail(userDto.getEmail());
		user.setName(userDto.getName());
		user.setPassword(userDto.getPassword());
		if (Objects.nonNull(userDto.getPhones())) {
			for (PhoneDto phoneDto: userDto.getPhones()) {
				PhoneEntity phone = new PhoneEntity(phoneDto.getNumber(), phoneDto.getCitycode(), phoneDto.getCountrycode(), user);
				phones.add(phone);
			}
		}

		user.setPhones(phones);
		
		return user;
	}
	
	
	public UserResponseDto userToUserResponseDto(UserEntity user) {
		UserResponseDto dto = new UserResponseDto();
		dto.setCreated(user.getCreated());
		dto.setModified(user.getModified());
		dto.setLastLogin(user.getLastLogin());
		dto.setToken(user.getToken().toString());
		dto.setId(user.getIdUser().toString());
		dto.setActive(user.getIsActive());
		return dto;
	}

}

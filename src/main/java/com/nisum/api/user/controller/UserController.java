package com.nisum.api.user.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nisum.api.user.dto.ResponseDto;
import com.nisum.api.user.dto.UserRequestDto;
import com.nisum.api.user.dto.UserResponseDto;
import com.nisum.api.user.service.UserService;
import com.nisum.api.user.utils.Constantes;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;

import jakarta.validation.Valid;
import jakarta.validation.ValidationException;

@RestController
@RequestMapping("/user")
public class UserController {

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping
	public ResponseEntity<ResponseDto<?>> save(@Valid @RequestBody UserRequestDto userRequest) {
		ResponseDto<UserResponseDto> response = new ResponseDto<UserResponseDto>().responseOk();
		try {
			UserResponseDto userResponse = userService.save(userRequest);
			response.setEntidad(userResponse);
			return ResponseEntity.ok(response);	
		}
		catch (Exception e) {
			response = new ResponseDto<UserResponseDto>().responseError();
			if (e instanceof DataIntegrityViolationException) {	
				response.setMensaje(Constantes.MSJ_CORREO_EXISTE);
				return ResponseEntity.badRequest().body(response);	
			}
			if (e instanceof ValidationException) {
				response.setMensaje(e.getMessage());
				return ResponseEntity.badRequest().body(response);	
			}
			return ResponseEntity.internalServerError().body(response);	
		}
	
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
}

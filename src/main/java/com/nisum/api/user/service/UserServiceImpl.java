package com.nisum.api.user.service;

import java.util.Date;
import java.util.UUID;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nisum.api.user.dto.UserRequestDto;
import com.nisum.api.user.dto.UserResponseDto;
import com.nisum.api.user.entity.UserEntity;
import com.nisum.api.user.parser.UserParser;
import com.nisum.api.user.repository.UserRepository;
import com.nisum.api.user.utils.Constantes;

import jakarta.validation.ValidationException;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final UserParser userParser;
	
	@Value("${my.regex.pattern}")
	private String passwordPattern;

	public UserServiceImpl(UserRepository userRepository, UserParser userParser) {
		this.userRepository = userRepository;
		this.userParser = userParser;
	}

	@Override
	public UserResponseDto save(UserRequestDto userRequest) throws Exception {
		validatePassword(userRequest);
		UserEntity userToSave = userParser.userDtoToUserEntity(userRequest);
		userToSave.setCreated(new Date());
		userToSave.setLastLogin(new Date());
		userToSave.setToken(UUID.randomUUID());
		userToSave.setIsActive(Boolean.TRUE);
		UserEntity user = userRepository.save(userToSave);
		return userParser.userToUserResponseDto(user);
	}
	
	private void validatePassword(UserRequestDto userRequest) throws Exception {
	    if (!Pattern.matches(passwordPattern, userRequest.getPassword().toString())) {
	    	System.out.println("error!: " + userRequest.getPassword() + " : " + passwordPattern);
	        throw new ValidationException(Constantes.MSJ_PASSWORD_NOCUMPLE);
	    }
	}

}

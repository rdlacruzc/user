package com.nisum.api.user.service;

import com.nisum.api.user.dto.UserRequestDto;
import com.nisum.api.user.dto.UserResponseDto;

public interface UserService {

	UserResponseDto save(UserRequestDto userRequest) throws Exception;
}

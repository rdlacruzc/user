package com.nisum.api.user.dto;

public class ResponseDto<K> {

	private Object entidad;
	private String mensaje;

	public Object getEntidad() {
		return entidad;
	}

	public void setEntidad(Object entidad) {
		this.entidad = entidad;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public ResponseDto<K> responseOk() {
		ResponseDto<K> response = new ResponseDto<>();
		response.setMensaje("Se realizó correctamente");
		return response;
	}

	public ResponseDto<K> responseError() {
		ResponseDto<K> response = new ResponseDto<>();
		response.setMensaje("Ocurrió un error");
		return response;
	}

}